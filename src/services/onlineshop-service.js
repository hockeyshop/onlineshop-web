export default class OnlineShopService {
    
	categories = [
		{
			id: 1,
			name: "Pants",
			urlName: "pants",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/37196/41/126588",
			active: false,
		},
		{
			id: 2,
			name: "Sticks",
			urlName: "sticks",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/29896/41/112939",
			active: false,
		},
		{
			id: 3,
			name: "Skates",
			urlName: "skates",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/37210/41/124434",
			active: false,
		},
		{
			id: 4,
			name: "Gloves",
			urlName: "gloves",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/34944/41/118939",
			active: false,
		},
		{
			id: 5,
			name: "Helmets",
			urlName: "helmets",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/37321/2/127720",
			active: false,
		},
		{
			id: 6,
			name: "Leg pads",
			urlName: "legPads",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/37182/41/125203",
			active: false,
		},
		{
			id: 7,
			name: "Elbow Pads",
			urlName: "elbowPads",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/34193/41/128557",
			active: false,
		},
		{
			id: 8,
			name: "Shoulder Pads",
			urlName: "shoulderPads",
			img:
				"https://media.purehockey.com/q_auto,f_auto,fl_lossy,c_lpad,b_auto,w_400,h_400/products/22991/41/129162",
			active: false,
		},
    ];
    
	getCategories() {
		return new Promise((resolve) => {
			setTimeout(() => {
				resolve(this.categories);
			}, 700);
		});
	}
}
