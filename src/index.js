import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import reducers from "./store/rootReducer";

import ErrorBoundry from "./components/error-boundry/error-boundry";
import OnlineShopService from "./services/onlineshop-service";
import { OnlineShopServiceProvider } from "./components/onlineshop-service-context/onlineshop-service-context";

const onlineshopService = new OnlineShopService();

const store = createStore(
	reducers,
	composeWithDevTools(applyMiddleware(logger))
);

const products = require("./MyData.json");
store.dispatch({ type: "SET_PRODUCTS", payload: products });

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<ErrorBoundry>
				<OnlineShopServiceProvider value={onlineshopService}>
					<App />
				</OnlineShopServiceProvider>
			</ErrorBoundry>
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);

serviceWorker.unregister();
