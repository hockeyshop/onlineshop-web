import React from "react";
import "./categoryFilter.scss";
import { NavLink } from "react-router-dom";

function CategoryFilter({ categories, selectElem }) {
	return (
		<div className="categories">
			<p className="categories_head">
				<NavLink to="/categories" onClick={() => selectElem("categories")}>
					Categories
				</NavLink>
			</p>
			<ul>
				{categories.map((category) => (
					<li key={category.id}>
						<NavLink
							to={`/categories/${category.urlName}`}
							className="categories_list_item_lnk"
							onClick={() => selectElem(category.name)}>
							{category.name}
						</NavLink>
					</li>
				))}
			</ul>
		</div>
	);
}

export default CategoryFilter;
