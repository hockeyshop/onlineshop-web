import React from 'react';

const {
  Provider: OnlineShopServiceProvider,
  Consumer: OnlineShopServiceConsumer
} = React.createContext();

export {
    OnlineShopServiceProvider,
    OnlineShopServiceConsumer
};
