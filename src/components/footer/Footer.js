import React from "react";
import "./footer.scss";

function Footer() {
	return (
		<div className="footer">
			<span className="footer_text">Кузьмин А.А. &nbsp; 2020</span>
			<span className="footer_text">
				Prices are taken from the{" "}
				<a href="https://www.purehockey.com">Purehockey.com</a>
			</span>
		</div>
	);
}

export default Footer;
