import React from "react";
import "./style-PriceFilter.scss";

class PriceFilter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			inStock: this.props.filterParam.stock,
			minPrice: this.props.filterParam.minPrice,
			maxPrice: this.props.filterParam.maxPrice,
		};
		this.setFilter = this.setFilter.bind(this);
	}

	setFilter(event, props) {
		event.preventDefault();
		this.props.setMinPrice(+this.state.minPrice);
		this.props.setMaxPrice(+this.state.maxPrice);
		this.props.setStockFilter(this.state.inStock);
	}

	render() {
		return (
			<form className="filters" onSubmit={this.setFilter}>
				<p className="filters_head">Filters</p>
				<div className="checkbox_in_stock">
					<label htmlFor="checkbox">In stock</label>
					<input
						className="checkbox_fild"
						id="checkbox"
						name="checkbox"
						type="checkbox"
						defaultChecked={this.state.inStock}
						onChange={() =>
							this.setState({ inStock: !this.state.inStock })
						}></input>
				</div>
				<div className="min_max_price">
					<p className="min_max_price_head">Price range</p>
					<input
						className="min_price"
						id="min_price"
						name="min_price"
						type="number"
						value={this.state.minPrice}
						onChange={(event) =>
							this.setState({ minPrice: event.target.value })
						}
						step="10"
						min="0"
						max="60000"></input>
					<span>&nbsp; - &nbsp;</span>
					<input
						className="max_price"
						id="max_price"
						name="max_price"
						type="number"
						value={this.state.maxPrice}
						onChange={(event) =>
							this.setState({ maxPrice: event.target.value })
						}
						step="10"
						min="0"
						max="2000"></input>
				</div>
				<input
					className="apply_button"
					type="submit"
					value="Apply filters"></input>
			</form>
		);
	}
}

export default PriceFilter;
