import React from "react";
import "./basketItem.scss";

class BasketItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			totalPrice: this.props.item.quantity * this.props.item.product.price,
			quantity: this.props.item.quantity,
			maxQuantity: this.props.maxQuantity + this.props.item.quantity,
		};
		this.changeQuantity = this.changeQuantity.bind(this);
	}

	changeQuantity(event, value) {
		event.preventDefault();
		if (+value > this.state.maxQuantity) {
			alert(
				"Данного товара доступно к покупке не более: " +
					this.props.maxQuantity +
					" штук"
			);
		}

		this.props.changeQuantityPurchase(event, {
			...this.props.item,
			quantity: value - this.state.quantity,
		});
		this.setState(() => ({
			quantity: value,
			totalPrice: value * this.props.item.product.price,
		}));
	}

	render() {
		const item = this.props.item;
		return (
			<div className="product_order" key={item.product.id}>
				<div className="order_img_blok1">
					<img src={item.product.img} className="oimg" alt=""></img>
				</div>
				<div className="order_blok2">
					<p className="order_head">{item.product.name}</p>
					<p className="order_data">Price: ${item.product.price}</p>
					<div className="order_data">
						Quantity ordered:
						<input
							className="quantity_in_order"
							type="number"
							value={this.state.quantity}
							onChange={(event) =>
								this.changeQuantity(event, event.target.value)
							}
							min="0"
							max={this.state.maxQuantity}></input>
					</div>
					<p className="order_data">Total price: ${this.state.totalPrice}</p>
					<input
						className="input_buy"
						type="submit"
						value="Del product"
						// onClick={}
					></input>
					<input
						className="input_buy"
						type="submit"
						value="Buy product"
						// onClick={}
					></input>
				</div>
			</div>
		);
	}
}

export default BasketItem;
