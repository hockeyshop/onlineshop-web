import React from "react";
import { NavLink } from "react-router-dom";
import "./headerElems.scss";
import logo from "./../../img/basket.png";


const HeadName = () => {
	return (
		<div className="head_name_block">
			<p className="head_name_1">Hockey</p>
			<p className="head_name_2">OnlineShop</p>
		</div>
	);
};

const Login = () => {
	return (
		<NavLink to="/login" className="login">
			Login
		</NavLink>
	);
};

const Account = () => {
	return (
		<NavLink to="/account" className="account">
			Account
		</NavLink>
	);
};

const Basket = () => {
	return (
		<NavLink to="/basket" className="basket">
			<img className="basket_img" src={logo} alt=""></img>
		</NavLink>
	);
};

export {HeadName, Login, Account, Basket}