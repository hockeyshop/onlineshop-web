import React from "react";
import "./breadcrumb.scss";
import { NavLink } from "react-router-dom";

function BreadcrumbsItemList(bcArray) {
	bcArray.splice(0, 1);
	if (bcArray[0].length === 0) {
		bcArray[0] = "Main";
	}
	bcArray = bcArray.map((item) => item[0].toUpperCase() + item.slice(1));
	if (bcArray.length === 2) {
		bcArray.splice(1, 0, "--");
	}
	return bcArray.map((item, i) => (
		<span className="header_link" key={i}>
			<NavLink to={`/categories/${item}`} className="bc_elem activePage">
				{item}
			</NavLink>
		</span>
	));
}

function Breadcrumb(props) {
	let urlString = props.location.pathname;
	let bcArray = urlString.split("/", 3);

	return <ul className="breadcrumb">{BreadcrumbsItemList(bcArray)}</ul>;
}

export default Breadcrumb;
