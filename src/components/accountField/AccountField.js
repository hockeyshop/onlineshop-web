import React from "react";
import "./accountField.scss";

function AccountField({ field }) {
	return (
		<div className="account_field_li">
			<label className="data_label" htmlFor={field.id}>
				{field.name}
			</label>
			<input
				className={field.id === "addressField" ? "address_field" : "data_field"}
				id={field.id}
				type={field.type}
				name={field.id}
			/>
		</div>
	);
}

export default AccountField;
