import React from "react";
import { NavLink } from "react-router-dom";
import "./listItem.scss";

function ListItem({ item, selectElem, setCatFilter }) {
	const type = item.price ? "product" : "category";
	const divClassName = `${type}_block`;
	const imgClassName = `${type}_img`;
	const link = `/categories/${item.urlName}`;

	return (
		<div className={divClassName} key={item.id}>
			<NavLink to={link} onClick={() => selectElem(item.urlName)}>
				<p className="block_head">{item.name} </p>
				<img src={item.img} className={imgClassName} alt=""></img>
			</NavLink>
			{item.price && <p className="prod_price">Price: $ {item.price}</p>}
		</div>
	);
}

export default ListItem;
