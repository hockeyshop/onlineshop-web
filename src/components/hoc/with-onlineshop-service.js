import React from "react";
import { OnlineShopServiceConsumer } from "../onlineshop-service-context/onlineshop-service-context";

const withOnlineShopService = () => (Wrapped) => {
	return (props) => {
		return (
			<OnlineShopServiceConsumer>
				{(onlineshopService) => {
					return <Wrapped {...props} onlineshopService={onlineshopService} />;
				}}
			</OnlineShopServiceConsumer>
		);
	};
};

export default withOnlineShopService;
