import React from "react";
import { Route, BrowserRouter,Redirect } from "react-router-dom";
import "./style.scss";

import  withOnlineShopService  from './components/hoc/with-onlineshop-service'

import Header from "./containers/header/Header";
import Breadcrumb from "./components/breadcrumb/Breadcrumb";
import Aside from "./containers/aside/Aside";
import Footer from "./components/footer/Footer";
import Categories from "./containers/categories/Categories";
import ProductList from "./containers/productList/ProductList";
import Product from "./containers/product/Product";
import Account from "./pages/account/Account";
import Login from "./pages/login/Login";
import Basket from "./containers/basket/Basket";

function App({onlineshopService}) {
	return (
		<div className="wrapper">
			<BrowserRouter>
				<Route path="/" component={Header} />
				<Route path="/:path?" component={Breadcrumb} />
				<Route path="/" component={Aside} />
				<Redirect from='/' to='/categories' />
				<Route exact path="/categories" component={Categories} />
				<Route exact path="/categories/:cat" component={ProductList} />
				<Route
					path="/categories/:cat/:prod"
					render={(props) => <Product {...props} />}
				/>
				<Route path="/login" component={Login} />
				<Route path="/account" component={Account} />
				<Route path="/basket" render={(props) => <Basket {...props} />} />
				<Route path="/" component={Footer} />
			</BrowserRouter>
		</div>
	);
}

export default withOnlineShopService()(App);
