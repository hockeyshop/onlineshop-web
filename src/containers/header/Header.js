import React from "react";
import { connect } from "react-redux";

import "./header.scss";
import headerElems from "../../components/headerElems";

function Header({ loginState }) {
	const { HeadName, Login, Account, Basket } = headerElems;
	return (
		<div className="header">
			<HeadName />
			<div className="header_links">
				{loginState ? (
					<div className="header_link">
						<Account />
						<Basket />
					</div>
				) : (
					<div className="header_link">
						<Login />
					</div>
				)}
			</div>
		</div>
	);
}

const mapStateToProps = (store) => {
	return {
		loginState: store.loginState,
	};
};

export default connect(mapStateToProps)(Header);
