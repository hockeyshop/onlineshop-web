import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "./aside.scss";

import CategoryFilter from "../../components/categoryFilter/CategoryFilter";
import PriceFilter from "../../components/priceFilter/PriceFilter";

import selectElem from "../../store/actions/activeElem-action";
import * as filterActions from "../../store/actions/filter-action";

const checkUrl = (urlString) => {
	const urlNotVisible = ["product", "login", "account", "basket"];
	return (
		urlNotVisible.some((url) => urlString.indexOf(url) > 0) || urlString === "/"
	);
};

function Aside(props) {
	let urlString = props.location.pathname;

	return (
		<div className="aside">
			<CategoryFilter
				categories={props.categories}
				selectElem={props.selectElem}
			/>
			{!checkUrl(urlString) && (
				<PriceFilter
					filterParam={props.filterParam}
					setMinPrice={props.setMinPrice}
					setMaxPrice={props.setMaxPrice}
					setStockFilter={props.setStockFilter}
				/>
			)}
		</div>
	);
}

function mapStateToProps(store) {
	return {
		activeElem: store.activeElem,
		categories: store.categories,
		filterParam: store.filter,
	};
}

const mapDispatchToProps = (dispatch) => ({
	...bindActionCreators({ ...filterActions, selectElem }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Aside);
