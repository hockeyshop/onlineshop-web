import React from "react";
import { connect } from "react-redux";
import "./basket.scss";

import BasketItem from "../../components/basketItem/BasketItem";

import { addPurchase, delPurchase } from "../../store/actions/basket-action";
import { changeQuantityProd } from "../../store/actions/products_action";

class Basket extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.changeQuantityPurchase = this.changeQuantityPurchase.bind(this);
		this.totalPriceOrder = this.totalPriceOrder.bind(this);
		this.maxQuantity = this.maxQuantity.bind(this);
	}

	changeQuantityPurchase(event, purchase) {
		event.preventDefault();
		this.props.addPurchase(purchase);
		this.props.changeQuantityProd({ ...purchase, quantity: purchase.quantity });
	}

	componentDidMount() {
		this.totalPriceOrder(this.state.totalPrice);
	}

	totalPriceOrder() {
		return this.props.basket.reduce((total, item) => {
			return total + item.product.price * item.quantity;
		}, 0);
	}

	maxQuantity(id) {
		const product = this.props.products.find((product) => product.id === id);
		return product.quantity;
	}

	render() {
		return (
			<div className="orders_block">
				{this.props.basket.map((item) => (
					<BasketItem
						key={item.product.id}
						item={item}
						maxQuantity={this.maxQuantity(item.product.id)}
						changeQuantityPurchase={this.changeQuantityPurchase}
						totalPriceOrder={this.totalPriceOrder}
					/>
				))}
				{this.totalPriceOrder() !== 0 && (
					<div>
						<div className="totalPriceField">
							Total price order: &nbsp; ${this.totalPriceOrder()}
						</div>
						<input
							className="input_buy_all"
							type="submit"
							value="Buy all products"
							// onClick={}
						></input>
					</div>
				)}
			</div>
		);
	}
}

function mapStateToProps(store) {
	return {
		basket: store.basket,
		products: store.products,
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		addPurchase: (purchase) => dispatch(addPurchase(purchase)),
		delPurchase: (purchase) => dispatch(delPurchase(purchase)),
		changeQuantityProd: (changeData) =>
			dispatch(changeQuantityProd(changeData)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
