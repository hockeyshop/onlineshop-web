import React from "react";
import "./style-ProductList.scss";
import ListItem from "../../components/listItem/ListItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import selectElem from "../../store/actions/activeElem-action";
import * as filterActions from "../../store/actions/filter-action";

function ProductList(props) {
	const selectElem = props.selectElem;

	let cat = props.match.params.cat;
	let products = props.products.filter(
		(prod) =>
			prod.category === cat &&
			prod.price <= props.filterParam.maxPrice &&
			prod.price >= props.filterParam.minPrice
	);

	if (props.filterParam.stock) {
		products = products.filter((prod) => prod.quantity > 0);
	}

	products.forEach(
		(product) => (product.urlName = `${product.category}/${product.id}`)
	);

	return (
		<div className="products_blocks">
			{products.map((item) => (
				<ListItem item={item} selectElem={selectElem} key={item.id} />
			))}
		</div>
	);
}

function mapStateToProps(store) {
	return {
		activeElem: store.activeElem,
		filterParam: store.filter,
		products: store.products,
	};
}

const mapDispatchToProps = (dispatch) => ({
	...bindActionCreators({ ...filterActions, selectElem }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
