import React from "react";
import { connect } from "react-redux";
import "./style-Product.scss";

import { addPurchase } from "../../store/actions/basket-action";
import { changeQuantityProd } from "../../store/actions/products_action";

class Product extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			quantity: 0,
		};
		this.addToCart = this.addToCart.bind(this);
	}

	addToCart(event, product, props) {
		event.preventDefault();

		if (product.quantity === 0) {
			alert("Данного товара нет в наличии");
			this.setState({ quantity: 0 });
		} else {
			if (product.quantity - this.state.quantity < 0) {
				alert(
					"Данного товара доступно к покупке не более: " +
						product.quantity +
						" штук"
				);
				this.setState({ quantity: product.quantity });
			} else {
				const purchase = {
					product: product,
					quantity: Number(this.state.quantity),
				};
				this.props.addPurchase(purchase);
				this.props.changeQuantityProd(purchase);
				this.setState({ quantity: 0 });
			}
		}
	}

	findProduct(products) {
		return products.find((prod) => prod.id === +this.props.match.params.prod);
	}

	render() {
		const product = this.props.products[this.props.match.params.prod - 1];
		const maxQuantity = product.quantity;
		const quantityByString = (q) =>
			q >= 10 ? "lot" : q === 0 ? "empty" : "few";
		return (
			<form className="product">
				<div className="blok1">
					<img
						src={product.img}
						className="img_prod"
						alt=""
						onClick={() => window.open(`${product.link}`, "_blank")}></img>
				</div>
				<div className="blok2">
					<p className="blok_head">{product.name}</p>
					<div className="description_product">
						<p>{product.description}</p>
					</div>
				</div>
				{this.props.loginState && (
					<div className="blok3">
						<p className="price">Price: ${product.price}</p>
						<p className="prod_quantity">
							Stock level: {quantityByString(product.quantity)}
						</p>
						<input
							className="quantity_prod_buy"
							id="quantity_prod_buy"
							name="quantity_prod_buy"
							type="number"
							onChange={(event) =>
								this.setState({ quantity: event.target.value })
							}
							value={this.state.quantity}
							min="0"
							max={maxQuantity}></input>
						<input
							className="buy_button"
							type="submit"
							value="Buy product"
							onClick={(event) => this.addToCart(event, product)}></input>
					</div>
				)}
			</form>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		basket: store.basket,
		activeElem: store.activeElem,
		loginState: store.loginState,
		products: store.products,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		addPurchase: (purchase) => dispatch(addPurchase(purchase)),
		changeQuantityProd: (changeData) =>
			dispatch(changeQuantityProd(changeData)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
