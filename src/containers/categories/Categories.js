import React from "react";
import "./categories.scss";
import ListItem from "../../components/listItem/ListItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import withOnlineShopService from "./../../components/hoc/with-onlineshop-service";
import compose from "./../../utils/compose";

import selectElem from "../../store/actions/activeElem-action";
import { categoriesLoaded } from "../../store/actions/categories-action";
import * as filterActions from "../../store/actions/filter-action";

class Categories extends React.Component {
	componentDidMount() {
		const { onlineshopService, categoriesLoaded } = this.props;
		onlineshopService
			.getCategories()
			.then((categories) => categoriesLoaded(categories));
	}

	render() {
		const activeElem = this.props.activeElem;
		const items = this.props[activeElem];
		const selectElem = this.props.selectElem;

		return (
			<div className="categories_bloсks">
				{items.map((item) => (
					<ListItem item={item} selectElem={selectElem} key={item.id} />
				))}
			</div>
		);
	}
}

function mapStateToProps(store) {
	return {
		activeElem: store.activeElem,
		categories: store.categories,
		products: store.products,
	};
}

const mapDispatchToProps = (dispatch) => ({
	...bindActionCreators(
		{ ...filterActions, selectElem, categoriesLoaded },
		dispatch
	),
});

export default compose(
	withOnlineShopService(),
	connect(mapStateToProps, mapDispatchToProps)
)(Categories);
