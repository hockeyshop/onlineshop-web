
export function setProducts(products) {
	return {
		type: "SET_PRODUCTS",
		payload: products,
	}
}

export function addProduct(product) {
	return {
		type: "ADD_PRODUCT",
		payload: product,
	}
}

export function changeQuantityProd(changeData) {
	return {
		type: "CHANGE_QUANTITY_PRODUCT",
		payload: changeData,
	}
}
