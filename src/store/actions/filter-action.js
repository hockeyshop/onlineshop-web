export const setCategoryFilter = (cat) => {
	return {
		type: "SET_CATEGORY",
		payload: cat,
	};
};

export const setStockFilter = (bool) => {
	return {
		type: "SET_STOCK",
		payload: bool,
	};
};

export const setMinPrice = (minPrice) => {
	return {
		type: "SET_MIN_PRICE",
		payload: minPrice,
	};
};

export const setMaxPrice = (maxPrice) => {
	return {
		type: "SET_MAX_PRICE",
		payload: maxPrice,
	};
};
