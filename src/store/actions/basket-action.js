
export function addPurchase(purchase) {
	return {
		type: "ADD_PURCHASE",
		payload: purchase,
	}
}

export function delPurchase(purchase) {
	return {
		type: "DEL_PURCHASE",
		payload: purchase,
	}
}
