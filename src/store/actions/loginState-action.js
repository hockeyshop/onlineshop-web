
export function setLoginState() {
    	return {
		type: "SET_LOGIN_STATE",
	}
}

export function setLogoutState() {
	return {
	type: "SET_LOGOUT_STATE",
}
}
