
export default function selectElem(elem) {
	return {
		type: "ELEM_SELECTED",
		payload: elem,
	}
}
