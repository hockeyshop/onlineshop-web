
export const categoriesLoaded = (categories) => {
	return {
		type: "CATEGORIES_LOADED",
		payload: categories,
	};
};

// export function categoriesFetchData(url) {
//     return (dispatch) => {
//         fetch(url)
//             .then(response => {
//                 if(!response.ok) {
//                     throw new Error(response.statusText);
//                 }
//                 return response;
//             })
//             .then(response => response.json())
//             .then(categories => dispatch(categoriesLoaded(categories)))
//             .catch(()=>{});
//     }
// }
