import { combineReducers } from "redux";
import categoriesReducer from "./reducers/categories-reducer";
import activeElemReducer from "./reducers/activeElem-reducer";
import filterReducer from "./reducers/filter-reducer";
import basketReducer from "./reducers/basket-reducer";
import productsReducer from "./reducers/products-reducer";
import loginStateReducer from "./reducers/loginState-reducer";


const rootReducer = combineReducers({
	products: productsReducer,
	filter: filterReducer,
	activeElem: activeElemReducer,
	categories: categoriesReducer,
	basket: basketReducer,
	loginState: loginStateReducer,
});

export default rootReducer;
