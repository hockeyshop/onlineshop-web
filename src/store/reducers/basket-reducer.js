const basket = [];

export default function basketReducer(state = basket, action = {}) {
	switch (action.type) {
		case "ADD_PURCHASE":
			let newPurchase = action.payload;
			let purchaseIdx = state.findIndex(
				(p) => p.product.id === +newPurchase.product.id
			);

			if (purchaseIdx < 0) {
				return [...state, newPurchase];
			}

			const oldPurchase = state[purchaseIdx];
			newPurchase = {
				...oldPurchase,
				quantity: oldPurchase.quantity + newPurchase.quantity,
			};
			return [
				...state.slice(0, purchaseIdx),
				newPurchase,
				...state.slice(purchaseIdx + 1),
			];
		case "DEL_PURCHASE":
		default:
			return state;
	}
}
