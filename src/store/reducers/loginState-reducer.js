const loginState = true;

export default function loginStateReducer(state = loginState, action = {}) {
	switch (action.type) {
		case "SET_LOGIN_STATE":
			return (state = true);
		case "SET_LOGOUT_STATE":
			return (state = false);
		default:
			return state;
	}
}
