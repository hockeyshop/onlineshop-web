const initialState = 'categories'

export default function activeElemReducer(state = initialState, action) {
	switch (action.type) {
		case "ELEM_SELECTED":
			return action.payload ;
		default:
			return state;
	}
}
