const products = [];

export default function productsReducer(state = products, action = {}) {
	switch (action.type) {
		case "SET_PRODUCTS":
			return (state.products = action.payload);
		case "ADD_PRODUCT":
			return { ...state, products: products.push(action.payload) };
		case "CHANGE_QUANTITY_PRODUCT":
			const product = action.payload.product;
			const productIdx = state.findIndex(
				(prod) => prod.id === product.id
			);
			const oldProduct = state[productIdx];
			const newProduct = {
				...product,
				quantity: oldProduct.quantity - action.payload.quantity,
			};
			return [
				...state.slice(0, productIdx),
				newProduct,
				...state.slice(productIdx + 1),
			];
			
		default:
			return state;
	}
}
