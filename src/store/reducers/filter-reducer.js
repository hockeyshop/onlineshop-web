const initialState = {
	cat: 'pants',
	stock: false,
	minPrice: 10,
	maxPrice: 2000,
};

export default function filterReducer(state = initialState, action) {
	switch (action.type) {
		case "SET_CATEGORY":
			return { ...state, cat: action.payload };
		case "SET_STOCK":
			return { ...state, stock: action.payload };
		case "SET_MIN_PRICE":
			return { ...state, minPrice: action.payload };
		case "SET_MAX_PRICE":
			return { ...state, maxPrice: action.payload };
		default:
			return state;
	}
}
