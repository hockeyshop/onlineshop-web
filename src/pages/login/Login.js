import React from "react";
import { connect } from "react-redux";

import "./login.scss";
import AccountField from "../../components/accountField/AccountField";
import { setLoginState } from "./../../store/actions/loginState-action";
import selectElem from "./../../store/actions/activeElem-action";

const fields = [
	{ name: "Login", id: "loginField", type: "text" },
	{ name: "Password", id: "passwordField", type: "password" },
];

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.loginUser = this.loginUser.bind(this);
	}

	loginUser(event) {
		event.preventDefault();
		this.props.setLoginState();
		this.props.selectElem("categories");
		alert("You successfull login");
		this.props.history.push("/categories");
	}

	render() {
		return (
			<div className="login_blok">
				<form>
					<ul>
						{fields.map((field) => (
							<AccountField field={field} key={field.id} />
						))}
					</ul>
					<input
						className="input_bottom"
						type="submit"
						value="Войти"
						onClick={this.loginUser}></input>
				</form>
			</div>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		loginState: store.loginState,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setLoginState: () => dispatch(setLoginState()),
		selectElem: (elem) => dispatch(selectElem(elem)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
