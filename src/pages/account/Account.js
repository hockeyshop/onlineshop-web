import React from "react";
import { connect } from "react-redux";

import "./account.scss";
import AccountField from "../../components/accountField/AccountField";
import { setLogoutState } from "./../../store/actions/loginState-action";
import selectElem from "./../../store/actions/activeElem-action";

const fields = [
	{ name: "First Name", id: "firstnameField", type: "text" },
	{ name: "Last Name", id: "lastnameField", type: "text" },
	{ name: "Patronymic", id: "patronymicField", type: "text" },
	{ name: "E-mail", id: "emailField", type: "email" },
	{ name: "Phone", id: "phoneField", type: "tel" },
	{ name: "Address", id: "addressField", type: "text" },
];

class Account extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.logoutUser = this.logoutUser.bind(this);
	}

	logoutUser(event) {
		event.preventDefault();
		this.props.setLogoutState();
		this.props.selectElem('categories');
		alert("You successfull exit");
		this.props.history.push("/categories");
	}

	render() {
		return (
			<div className="account_block">
				<form >
						{fields.map((field) => (
							<AccountField typeName={"account"} field={field} key={field.id} />
						))}
					<input
						className="input_bottom"
						type="submit"
						value="Сохранить"
						onClick={this.logoutUser}
					/>
				</form>
			</div>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		loginState: store.loginState,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setLogoutState: () => dispatch(setLogoutState()),
		selectElem: (elem) => dispatch(selectElem(elem)),
	};
};


export default connect(mapStateToProps, mapDispatchToProps)(Account);
